import sys

import click
from terminaltables import SingleTable

from electronics_calculator import resistor as r

VERSION = "0.2.0-dev0"


class AliasedGroup(click.Group):
    def get_command(self, ctx, cmd_name):
        rv = click.Group.get_command(self, ctx, cmd_name)
        if rv is not None:
            return rv
        matches = [x for x in self.list_commands(ctx) if x.startswith(cmd_name)]
        if not matches:
            return None
        elif len(matches) == 1:
            return click.Group.get_command(self, ctx, matches[0])
        ctx.fail("Too many matches: %s" % ", ".join(sorted(matches)))


@click.group(cls=AliasedGroup)
@click.version_option(VERSION)
def main():
    pass


@main.group(cls=AliasedGroup)
def resistor():
    pass


@resistor.command("value")
@click.argument("values", nargs=-1)
def value(values):
    """
    Determines the resistance value of a resistor from its marking.

    Given the coloured bands or the value printed on a resistor (in the case of an
    EIA-96 Surface Mount), will return the resistors value.

    This will not check to see if such a resistor matches value in the common
    resistor series (E12, E24, etc)
    """
    if len(values) == 1:
        # EIA-96
        result = r.eia_96_value(values[0])
    elif len(values) == 4 or len(values) == 5:
        result = r.banded_resistor_value(*values)

    else:
        click.echo(
            click.style("Invalid resistor values supplied.", fg="yellow"), err=True
        )
        sys.exit(1)

    table_data = [
        ["Bands", "Resistance (Ω)", "Tolerance (%)"],
        [
            ", ".join(values),
            f"{result['resistor_value']} ({result['short_format']})",
            result["tolerance"],
        ],
    ]

    table = SingleTable(table_data)

    click.echo(f"\n{table.table}")


@resistor.command("bands")
@click.argument("value", type=int)
@click.option(
    "--bands",
    type=int,
    default=4,
    help="Return results for 4 or 5 band resistors. 4 is the default.",
)
def bands(value, bands):
    """
    Determines the colour coding for a resistor given a resistance value.

    Since arbitrary resistance values don't typically exist, this will select
    the closest match from E12 and E24 series resistors.
    """
    e12_result = r.value_to_bands(value, bands, series="E12")
    e24_result = r.value_to_bands(value, bands, series="E24")

    table_data = [
        ["Original Value", "Series", "Preferred Value", "Bands"],
    ]

    e12_bands = [v for k, v in e12_result.items() if k.startswith("band")]
    e12_bands.append(e12_result["multiplier"])

    table_data.append(
        [value, "E12", e12_result["preferred_value"], ", ".join(e12_bands)]
    )

    e24_bands = [v for k, v in e24_result.items() if k.startswith("band")]
    e24_bands.append(e24_result["multiplier"])

    table_data.append(
        [value, "E24", e24_result["preferred_value"], ", ".join(e24_bands)]
    )

    table = SingleTable(table_data)

    click.echo(f"\n{table.table}")


@resistor.command("series")
@click.argument("values", nargs=-1, type=int)
def series(values):
    """
    Calculates the resulting resistance of series resistors.

    This uses the formula:

    Rtotal = R1 + R2 + ... + Rn
    """
    if len(values) < 2:
        click.echo("Not enough resistors specified. Needs at least 2.")
        sys.exit(1)

    result = r.calculate_series_resistance(*values)
    result = str(round(result, 2)).replace(".0", "")

    header = []
    row = []

    for i, value in enumerate(values, 1):
        header.append(f"R{i} (Ω)")
        row.append(value)

    header.append(click.style("Total Series Resistance (Ω)", fg="green"))
    row.append(click.style(result, fg="green"))

    table = SingleTable([header, row])

    click.echo(f"\n{table.table}")


@resistor.command("parallel")
@click.argument("values", nargs=-1, type=int)
def parallel(values):
    """
    Calculates resulting resistance of parallel resistors.

    This uses the formula:

    1 / Rtotal = 1/R1 + 1/R2 + ... + 1/Rn
    """

    if len(values) < 2:
        click.echo("Not enough resistors specified. Needs at least 2.")
        sys.exit(1)

    result = r.calculate_parallel_resistance(*values)
    result = str(round(result, 2)).replace(".0", "")

    header = []
    row = []

    for i, value in enumerate(values, 1):
        header.append(f"R{i} (Ω)")
        row.append(value)

    header.append(click.style("Total Parallel Resistance (Ω)", fg="green"))
    row.append(click.style(result, fg="green"))

    table = SingleTable([header, row])

    click.echo(f"\n{table.table}")


@resistor.command("vdivider")
@click.argument("r1", type=int)
@click.argument("r2", type=int)
@click.argument("source", type=int)
def vdivider(r1, r2, source):
    """
    Calculates the output voltage of a voltage divider. R1 is the resistor closest
    to SOURCE. R2 is the resistor closest to ground. SOURCE is the
    source voltage.

    12v ----|  R1  |----X----|  R2  |---- GND

    X is the output voltage that is calculated.
    """
    result = r.calculate_voltage_divider(r1, r2, source)
    result = str(round(result, 2)).replace(".0", "")

    table = SingleTable(
        [
            [
                "Source Voltage (V)",
                click.style("Output Voltage (V)", fg="green"),
                "R1 (Ω)",
                "R2 (Ω)",
            ],
            [source, click.style(result, fg="green"), r1, r2],
        ]
    )

    click.echo(f"\n{table.table}")


@resistor.command("divider_resistors")
@click.argument("source", type=int)
@click.argument("output", type=int)
def divider_resistors(source, output):
    """
    Calculates the resistors required in a voltage divider for a given
    SOURCE and required OUTPUT voltage.

    R1 is the resistor closest to the source voltage. R2 is the resistor
    closest to ground.
    """
    result = r.calculate_resistors_for_divider(source, output)

    exact_data = [
        "Exact Values",
        source,
        output,
        f"{result['r1']:.0f}",
        f"{result['r2']:.0f}",
    ]

    preferred = result["preferred_values"]
    preferred_data = [
        "E24 Series Values",
        source,
        f"{preferred['output_voltage']:.2f}",
        preferred["r1"],
        preferred["r2"],
    ]

    table = SingleTable(
        [
            ["", "Source Voltage (V)", "Output Voltage (V)", "R1 (Ω)", "R2 (Ω)"],
            exact_data,
            preferred_data,
        ]
    )

    click.echo(f"\n{table.table}")


if __name__ == "__main__":
    main()
