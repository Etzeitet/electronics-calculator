from bisect import bisect_left

from electronics_calculator.exceptions import InvalidResistorValue

BLACK = "black"
BROWN = "brown"
RED = "red"
ORANGE = "orange"
YELLOW = "yellow"
GREEN = "green"
BLUE = "blue"
VIOLET = "violet"
GREY = "grey"
WHITE = "white"
GOLD = "gold"
SILVER = "silver"

# COLOUR: (Band Value, Multiplier, Tolerance)
COLOURS = {
    BLACK: {"value": 0, "multiplier": 1, "tolerance": None},
    BROWN: {"value": 1, "multiplier": 10, "tolerance": 1},
    RED: {"value": 2, "multiplier": 100, "tolerance": 2},
    ORANGE: {"value": 3, "multiplier": 1000, "tolerance": None},
    YELLOW: {"value": 4, "multiplier": 10000, "tolerance": None},
    GREEN: {"value": 5, "multiplier": 100000, "tolerance": 0.5},
    BLUE: {"value": 6, "multiplier": 1000000, "tolerance": 0.25},
    VIOLET: {"value": 7, "multiplier": 10000000, "tolerance": 0.1},
    GREY: {"value": 8, "multiplier": 100000000, "tolerance": 0.05},
    WHITE: {"value": 9, "multiplier": 1000000000, "tolerance": None},
    GOLD: {"value": None, "multiplier": 0.1, "tolerance": 5},
    SILVER: {"value": None, "multiplier": 0.01, "tolerance": 10},
}

VALUE_TO_COLOURS = {
    0: BLACK,
    1: BROWN,
    2: RED,
    3: ORANGE,
    4: YELLOW,
    5: GREEN,
    6: BLUE,
    7: VIOLET,
    8: GOLD,
    9: WHITE,
}

EIA96 = {
    "01": 100,
    "02": 102,
    "03": 105,
    "04": 107,
    "05": 110,
    "06": 113,
    "07": 115,
    "08": 118,
    "09": 121,
    "10": 124,
    "11": 127,
    "12": 130,
    "13": 133,
    "14": 137,
    "15": 140,
    "16": 143,
    "17": 147,
    "18": 150,
    "19": 154,
    "20": 158,
    "21": 162,
    "22": 165,
    "23": 169,
    "24": 174,
    "25": 178,
    "26": 182,
    "27": 187,
    "28": 191,
    "29": 196,
    "30": 200,
    "31": 205,
    "32": 210,
    "33": 215,
    "34": 221,
    "35": 226,
    "36": 232,
    "37": 237,
    "38": 243,
    "39": 249,
    "40": 255,
    "41": 261,
    "42": 267,
    "43": 274,
    "44": 280,
    "45": 287,
    "46": 294,
    "47": 301,
    "48": 309,
    "49": 316,
    "50": 324,
    "51": 332,
    "52": 340,
    "53": 348,
    "54": 357,
    "55": 365,
    "56": 374,
    "57": 383,
    "58": 392,
    "59": 402,
    "60": 412,
    "61": 422,
    "62": 432,
    "63": 442,
    "64": 453,
    "65": 464,
    "66": 475,
    "67": 487,
    "68": 499,
    "69": 511,
    "70": 523,
    "71": 536,
    "72": 549,
    "73": 562,
    "74": 576,
    "75": 590,
    "76": 604,
    "77": 619,
    "78": 634,
    "79": 649,
    "80": 665,
    "81": 681,
    "82": 698,
    "83": 715,
    "84": 732,
    "85": 750,
    "86": 768,
    "87": 787,
    "88": 806,
    "89": 825,
    "90": 845,
    "91": 866,
    "92": 887,
    "93": 909,
    "94": 931,
    "95": 953,
    "96": 976,
    "Z": 0.001,
    "Y": 0.01,
    "R": 0.01,
    "X": 0.1,
    "S": 0.1,
    "A": 1,
    "B": 10,
    "H": 10,
    "C": 100,
    "D": 1000,
    "E": 10000,
    "F": 100000,
}

resistor_series = {
    "E12": [10, 12, 15, 18, 22, 27, 33, 39, 47, 56, 68, 82],
    "E24": [
        10,
        11,
        12,
        13,
        15,
        16,
        18,
        20,
        22,
        24,
        27,
        30,
        33,
        36,
        39,
        43,
        47,
        51,
        56,
        62,
        68,
        75,
        82,
        91,
    ],
}


def eia_96_value(value):
    """
    Calculates the value of an EIA-96 Surface Mount resitor.

    Args
    ====
    value: str, the 3-digit code printed on the EIA-96 resistor

    Returns
    =======
    A tuple containing resistor value, tolerance (1%), and short format.

    Raises
    ======
    InvalidResistorValue
    """
    if len(value) != 3:
        raise InvalidResistorValue(f"{value} is not 3 digits long.")

    digits = EIA96.get(value[:2])
    multiplier = EIA96.get(value[2])

    if digits is None or multiplier is None:
        raise InvalidResistorValue(f"{value} is not a valid EIA-96 code.")

    resistor_value = digits * multiplier
    tolerance = 1
    short_format = get_short_format(resistor_value)

    return {
        "resistor_value": resistor_value,
        "tolerance": tolerance,
        "short_format": short_format,
    }


def get_band_property(colour, property):
    if band := COLOURS.get(colour):
        return band[property]
    else:
        raise InvalidResistorValue(f"{colour} is not a valid resistor band colour.")


def banded_resistor_value(*args):
    """
    Calculates the resistor value and tolerence given the band colours.

    Args
    ====
    band1: str, first band colour
    band2: str, second band colour
    band3: str, third band colour
    band4: str, fourth band colour
    band5: str, fifth band colour (optional)

    Returns
    =======
    A tuple containing the resistor value, tolerance

    Raises
    ======
    InvalidResistorValue
    """

    if len(args) == 4:
        resistor_value_bands = args[0:2]
        multiplier_band = args[2]
        tolerance_band = args[3]
    else:
        resistor_value_bands = args[0:3]
        multiplier_band = args[3]
        tolerance_band = args[4]

    multiplier = get_band_property(multiplier_band, "multiplier")
    tolerance = get_band_property(tolerance_band, "tolerance")

    base_value = ""
    for band in resistor_value_bands:
        value = get_band_property(band, "value")
        base_value += str(value)

    resistor_value = int(base_value) * multiplier

    short_format = get_short_format(resistor_value)

    return {
        "resistor_value": resistor_value,
        "tolerance": tolerance,
        "short_format": short_format,
    }


def value_to_bands(value, bands=4, series="E12"):
    """
    This is the (almost) inverse of banded_resistor_value.

    This function will take a value of a resistor and return
    the colour bands to expect. Since resistors do not come in
    arbitrary values, this function will first convert the
    provided value to the closest equivalent in series.

    The default series is E12, but any valid series can be
    passed in.

    This function doesn't care about the tolerance band.

    Args
    ====
    value:  int, the resistance value to convert
    bands: int, the number of bands to convert to
    series: str, (optional) series to select the closest matching
            resistor from.

    Returns
    =======
    A dictionary with each of the coloured bands as keys, and their
    value/multiplier as values.
    """

    preferred = get_preferred_resistor(value, series)
    step_str = str(preferred["step"])
    step = preferred["step"]

    just_tens = preferred["resistor_value"] // step
    multiplier_value = len(str(just_tens)) - 1

    if bands == 4:
        band1 = VALUE_TO_COLOURS[int(step_str[0])]
        band2 = VALUE_TO_COLOURS[int(step_str[1])]
        multiplier = VALUE_TO_COLOURS[multiplier_value]

    return {
        "band1": band1,
        "band2": band2,
        "multiplier": multiplier,
        "preferred_value": preferred["resistor_value"],
    }


def calculate_series_resistance(*args):
    """
    Calculate series resistance.

    Args:
    =====
    args: 2 or more resistance values

    Returns
    =======
    float - total resistance

    Raises
    ======
    ValueError if not enough values in args
    """
    if len(args) < 2:
        raise ValueError("Requires at least two resistor values")

    return float(sum(args))


def calculate_parallel_resistance(*args):
    """
    Calculate parallel resistance.

    Args:
    =====
    args: 2 or more resistance values

    Returns
    =======
    float - total resistance

    Raises
    ======
    ValueError if not enough values in args
    """
    if len(args) < 2:
        raise ValueError("Requires at least two resistor values")

    reciprocal_sum = 0
    for value in args:
        reciprocal_sum += 1 / value

    return float(1 / reciprocal_sum)


def calculate_voltage_divider(r1, r2, voltage):
    """
    Calculates the output voltage from a voltage divider.

    This method assumes r1 is the resistor closest to the source
    voltage and r2 is closest to ground:

     VCC
      |
    +---+
    | r |
    | 1 |
    +---+
      |
      +-------- output voltage
      |
    +---+
    | r |
    | 2 |
    +---+
      |
     GND

    Args
    ====
    r1: numeric, "top" resistor value
    r2: numeric, "bottom" resistor value
    voltage: numeric, source voltage

    Returns
    =======
    float, the output voltage from the divider
    """
    output_v = (voltage * r2) / (r1 + r2)
    return float(output_v)


def calculate_resistors_for_divider(source, output):
    """
    Calculate the required resistances for the desired output voltage.

    This will output resistor sizes in the region of 10K as lower values
    may unnecessarily draw power from the source. However, what values
    are used are application specific as too-high a value will usually
    mean whatever component/device reads the output voltage will not get
    sufficient current to activate or take much longer (an ADC on an arduino
    for example.)

    Args
    ====
    source: numeric, the source voltage for the divider.
    output: numeric, the desired output voltage

    Returns
    =======
    An example pair of resistor values, R1 and R2, as well as their ratio.
    """

    voltage_ratio = output / source

    if voltage_ratio == 0.5:
        r1 = 10000
        r2 = (10000,)

    if voltage_ratio > 0.5:
        r2 = 10000
        r1 = ((source - output) * r2) / output

    else:
        r1 = 10000
        r2 = (output * r1) / (source - output)

    r1_preferred = get_preferred_resistor(r1, "E24")["resistor_value"]
    r2_preferred = get_preferred_resistor(r2, "E24")["resistor_value"]
    output_with_preferred = calculate_voltage_divider(
        r1_preferred, r2_preferred, source
    )

    result = {
        "r1": r1,
        "r2": r2,
        "voltage_ratio": voltage_ratio,
        "preferred_values": {
            "r1": r1_preferred,
            "r2": r2_preferred,
            "output_voltage": output_with_preferred,
        },
    }

    return result


def get_preferred_resistor(value, series="E12"):
    """
    """
    if value >= 100:
        value_step = int(str(value)[:2])
    else:
        value_step = value

    if r_series := resistor_series.get(series.upper()):

        pos = bisect_left(r_series, value_step)

        if pos == 0:
            preferred_step = r_series[0]

        elif pos == len(r_series):
            preferred_step = r_series[-1]

        else:
            # bisect_left will return the lower bound as the closest match
            # so this checks which step is actually closest to value_step
            before = r_series[pos - 1]
            after = r_series[pos]

            if after - value_step < value_step - before:
                preferred_step = after
            else:
                preferred_step = before

        tens = len(str(round(value))) - 2
        preferred_value = preferred_step * (10 ** tens)

        return {
            "resistor_value": preferred_value,
            "series": series.upper(),
            "short_format": get_short_format(preferred_value),
            "step": preferred_step,
        }


def get_short_format(value):
    if value < 1000:
        div = 1
        symbol = "R"

    elif value < 1000000:
        div = 1000
        symbol = "K"

    elif value < 1000000000:
        div = 1000000
        symbol = "M"

    elif value < 1000000000000:
        div = 1000000000
        symbol = "G"

    scaled_value = value / div

    if scaled_value < 1:
        return str(scaled_value) + "R"

    return str(scaled_value).replace(".0", symbol).replace(".", symbol)


if __name__ == "__main__":
    print(get_preferred_resistor(35000, "E24"))
