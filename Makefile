VERSION = 0.2.0-dev0

build:
	@echo 'Building PEX file version' $(VERSION)
	pex . -e electronics_calculator.ec:main --validate-entry-point --disable-cache -o dist/ec-$(VERSION).pex
