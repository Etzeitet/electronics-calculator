from setuptools import setup

NAME = "electronics_calculator"
VERSION = "0.2.0-dev0"
MODULES = ["electronics_calculator"]
REQUIRES = ["Click", "terminaltables"]

setup(
    name=NAME,
    version=VERSION,
    py_modules=MODULES,
    install_requires=REQUIRES,
    include_package_data=True,
    entry_points={"console_scripts": ["ec=electronics_calculator.ec:main"]},
)
