######################
Electronics Calculator
######################

This tool provides calculators for many common scenarios with basic
electronics.

The tool is in the very early stages and so far only provides calculations for
resistors. The full list is below.

This documentation will be updated/expanded as necessary.

Resistors
=========

All of the calculations available for resistors are found under the
``resistor`` command:

.. code::

    $ ec resistor

    Usage: ec resistor [OPTIONS] COMMAND [ARGS]...

    Options:
    --help  Show this message and exit.

    Commands:
    bands              Determines the colour coding for a resistor given a...
    divider_resistors  Calculates the resistors required in a voltage divider...
    parallel           Calculates resulting resistance of parallel resistors.
    series             Calculates the resulting resistance of series...
    value              Determines the resistance value of a resistor from its...
    vdivider           Calculates the output voltage of a voltage divider.


For more detailed help you can type a command and pass the ``--help`` option:

.. code::

    $ ec resistor value --help

Resistor Markings
-----------------

The ``value`` command will return the value of a resistor and its tolerance
based on
its markings. The tool currently supports 4 and 5 band through-hole resistors,
and EIA-96 Surface Mount resistors.

Examples:

.. code::

    $ ec resistor value red red red red  # 4 band resistor

    ┌────────────────────┬────────────────┬───────────────┐
    │ Bands              │ Resistance (Ω) │ Tolerance (%) │
    ├────────────────────┼────────────────┼───────────────┤
    │ red, red, red, red │ 2200 (2K2)     │ 2             │
    └────────────────────┴────────────────┴───────────────┘

    $ ec resistor value brown black green gold  # 5 band resistor

    ┌───────────────────────────┬────────────────┬───────────────┐
    │ Bands                     │ Resistance (Ω) │ Tolerance (%) │
    ├───────────────────────────┼────────────────┼───────────────┤
    │ brown, black, green, gold │ 1000000 (1M)   │ 5             │
    └───────────────────────────┴────────────────┴───────────────┘

    $ ec resistor value 33A  # EIA-96 surface-mount resistor

    ┌───────┬────────────────┬───────────────┐
    │ Bands │ Resistance (Ω) │ Tolerance (%) │
    ├───────┼────────────────┼───────────────┤
    │ 33A   │ 215 (215R)     │ 1             │
    └───────┴────────────────┴───────────────┘

The opposite command ``bands`` will do the reverse calculation - taking a value
and returning the coloured bands. This command will not return arbitrary
markings, but will instead work out the closest value resistor from both E12
and E24 series. There is no point returning markings for a resistor value
that doesn't exist!

This will not return the final tolerance band as while the tool can take an
educated guess (based on the series) it isn't likely to be accurate.

Note: This currently only works for 4-band resistors.

Examples:

.. code::

    $ ec resistor bands 1000000  # The value requested was already a standard stepping

    ┌────────────────┬────────┬─────────────────┬─────────────────────┐
    │ Original Value │ Series │ Preferred Value │ Bands               │
    ├────────────────┼────────┼─────────────────┼─────────────────────┤
    │ 1000000        │ E12    │ 1000000         │ brown, black, green │
    │ 1000000        │ E24    │ 1000000         │ brown, black, green │
    └────────────────┴────────┴─────────────────┴─────────────────────┘

    $ ec resistor bands 600000 # None-standard stepping, so tool returns closest alternatives

    ┌────────────────┬────────┬─────────────────┬─────────────────────┐
    │ Original Value │ Series │ Preferred Value │ Bands               │
    ├────────────────┼────────┼─────────────────┼─────────────────────┤
    │ 600000         │ E12    │ 560000          │ green, blue, yellow │
    │ 600000         │ E24    │ 620000          │ blue, red, yellow   │
    └────────────────┴────────┴─────────────────┴─────────────────────┘

Resistor Combinations
---------------------

The ``series`` and ``parallel`` commands provide calculations for combining
resistors.

Example:

.. code::

    $ ec resistor series

    ┌────────┬────────┬─────────────────────────────┐
    │ R1 (Ω) │ R2 (Ω) │ Total Series Resistance (Ω) │
    ├────────┼────────┼─────────────────────────────┤
    │ 2200   │ 330    │ 2530                        │
    └────────┴────────┴─────────────────────────────┘


.. code::

    $ ec resistor parallel 1000 4700

    ┌────────┬────────┬───────────────────────────────┐
    │ R1 (Ω) │ R2 (Ω) │ Total Parallel Resistance (Ω) │
    ├────────┼────────┼───────────────────────────────┤
    │ 1000   │ 4700   │ 824.56                        │
    └────────┴────────┴───────────────────────────────┘

Note: A future version may include a command to combine series and parallel
resistors, but that would take some careful planning on how you specify the
connections via cli!

Voltage Dividers
----------------

The ``vdivider`` command will output the output voltage of a voltage divider
when given two resistor values and source voltage. The order of the resistors
is R1 and R2, with R1 being the resistor closest to the source voltage.

Examples:

.. code::

    $ ec resistor vdivider 1000 330 10  # R1 = 1000, R2 = 330, source = 10V

    ┌────────────────────┬────────────────────┬────────┬────────┐
    │ Source Voltage (V) │ Output Voltage (V) │ R1 (Ω) │ R2 (Ω) │
    ├────────────────────┼────────────────────┼────────┼────────┤
    │ 10                 │ 2.48               │ 1000   │ 330    │
    └────────────────────┴────────────────────┴────────┴────────┘

    $ ec resistor vdivider 2200 10000 12  # R1 = 2K2, R2 = 10K, source = 12V

    ┌────────────────────┬────────────────────┬────────┬────────┐
    │ Source Voltage (V) │ Output Voltage (V) │ R1 (Ω) │ R2 (Ω) │
    ├────────────────────┼────────────────────┼────────┼────────┤
    │ 12                 │ 9.84               │ 2200   │ 10000  │
    └────────────────────┴────────────────────┴────────┴────────┘

The ``divider_resistors`` command will calculate the required resistor values
given the source voltage and required output voltage.

This command will do two things to determine the values. First, it works out
which of R1 and R2 should be the larger resistor and sets this to 10K Ohms. It
then calculates the value of the second resistor to get the desired output
voltage.

In order to select useful resistance values, it will limit itself to resistors
found in the E24 series. It will also display the expected output voltage when using
the standard values as a comparison with what was requested.

The exact resistances - even if not standard - are still
returned as it's possible to create a closer match with a combination of standard
resistors in series/parallel than trying to get the desired output voltage with
a single standard value.

For example, 3333 is not a standard value, and the closest standard value from
E24 is 3300. However, you can achieve 3333 by combining a 3300R and 33R
resistor.

Note: This command may be updated to calculate (simple!) resistor combinations
to get a closer match to the desired output voltage.

Examples:

.. code::

    $ ec resistor divider_resistors 10 5  # source = 10V, required output voltage = 5V

    ┌───────────────────┬────────────────────┬────────────────────┬────────┬────────┐
    │                   │ Source Voltage (V) │ Output Voltage (V) │ R1 (Ω) │ R2 (Ω) │
    ├───────────────────┼────────────────────┼────────────────────┼────────┼────────┤
    │ Exact Values      │ 10                 │ 5                  │ 10000  │ 10000  │
    │ E24 Series Values │ 10                 │ 5.00               │ 10000  │ 10000  │
    └───────────────────┴────────────────────┴────────────────────┴────────┴────────┘

    $ ec resistor divider_resistors 12 3  # source = 12V, required output voltage = 3V

    ┌───────────────────┬────────────────────┬────────────────────┬────────┬────────┐
    │                   │ Source Voltage (V) │ Output Voltage (V) │ R1 (Ω) │ R2 (Ω) │
    ├───────────────────┼────────────────────┼────────────────────┼────────┼────────┤
    │ Exact Values      │ 12                 │ 3                  │ 10000  │ 3333   │
    │ E24 Series Values │ 12                 │ 2.98               │ 10000  │ 3300   │
    └───────────────────┴────────────────────┴────────────────────┴────────┴────────┘
