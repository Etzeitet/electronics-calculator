#!/bin/bash

# Python and pyenv dependencies
sudo apt install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl git

curl https://pyenv.run | bash

export PATH="/home/parallels/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

pyenv install 3.8.0
pyenv local 3.8.0

python -m venv .venv
. .venv/bin/activate"
pip install --editable .

echo "Python 3.8.0 and application installed"
echo "Now run '. .venv/bin/activate' to use it."

