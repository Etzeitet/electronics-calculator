import pytest

from electronics_calculator import resistor
from electronics_calculator.exceptions import InvalidResistorValue

four_band_test_data = [
    ("brown", "black", "brown", "gold", (100, 5, "100R")),
    ("blue", "black", "yellow", "gold", (600000, 5, "600K")),
    ("red", "red", "red", "red", (2200, 2, "2K2")),
    ("red", "green", "red", "silver", (2500, 10, "2K5")),
    ("green", "violet", "green", "brown", (5700000, 1, "5M7")),
]

five_band_test_data = [
    ("red", "orange", "violet", "black", "brown", (237, 1, "237R")),
    ("blue", "red", "black", "brown", "brown", (6200, 1, "6K2")),
    ("brown", "black", "black", "yellow", "silver", (1000000, 10, "1M")),
    ("green", "red", "red", "red", "red", (52200, 2, "52K2")),
    ("grey", "grey", "orange", "yellow", "violet", (8830000, 0.1, "8M83")),
]

eia96_test_data = [
    ("34D", (221000, 1, "221K")),
    ("14Z", (0.137, 1, "0.137R")),
    ("11A", (127, 1, "127R")),
    ("75C", (59000, 1, "59K")),
    ("02F", (10200000, 1, "10M2")),
]


@pytest.mark.parametrize("a, b, c, d, expected", four_band_test_data)
def test_4_band(a, b, c, d, expected):
    result = resistor.banded_resistor_value(a, b, c, d)
    assert result["resistor_value"] == expected[0]
    assert result["tolerance"] == expected[1]
    assert result["short_format"] == expected[2]


@pytest.mark.parametrize("a, b, c, d, e, expected", five_band_test_data)
def test_5_band(a, b, c, d, e, expected):
    result = resistor.banded_resistor_value(a, b, c, d, e)
    assert result["resistor_value"] == expected[0]
    assert result["tolerance"] == expected[1]
    assert result["short_format"] == expected[2]


@pytest.mark.parametrize("a, expected", eia96_test_data)
def test_eia96(a, expected):
    result = resistor.eia_96_value(a)
    assert result["resistor_value"] == expected[0]
    assert result["tolerance"] == expected[1]
    assert result["short_format"] == expected[2]


def test_invalid_banded():
    with pytest.raises(InvalidResistorValue):
        resistor.banded_resistor_value("pink", "blue", "red", "silver")


def test_invalid_eia96():
    with pytest.raises(InvalidResistorValue):
        resistor.eia_96_value("98A")

    with pytest.raises(InvalidResistorValue):
        resistor.eia_96_value("02L")


value_to_e12_four_band_test_data = [
    (
        100,
        4,
        "E12",
        {
            "band1": "brown",
            "band2": "black",
            "multiplier": "brown",
            "preferred_value": 100,
        },
    ),
    (
        600000,
        4,
        "E12",
        {
            "band1": "green",
            "band2": "blue",
            "multiplier": "yellow",
            "preferred_value": 560000,
        },
    ),
    (
        2200,
        4,
        "E12",
        {"band1": "red", "band2": "red", "multiplier": "red", "preferred_value": 2200},
    ),
    (
        2500,
        4,
        "E12",
        {
            "band1": "red",
            "band2": "violet",
            "multiplier": "red",
            "preferred_value": 2700,
        },
    ),
    (
        5700000,
        4,
        "E12",
        {
            "band1": "green",
            "band2": "blue",
            "multiplier": "green",
            "preferred_value": 5600000,
        },
    ),
]

value_to_e24_four_band_test_data = [
    (
        100,
        4,
        "E24",
        {
            "band1": "brown",
            "band2": "black",
            "multiplier": "brown",
            "preferred_value": 100,
        },
    ),
    (
        600000,
        4,
        "E24",
        {
            "band1": "blue",
            "band2": "red",
            "multiplier": "yellow",
            "preferred_value": 620000,
        },
    ),
    (
        2200,
        4,
        "E24",
        {"band1": "red", "band2": "red", "multiplier": "red", "preferred_value": 2200},
    ),
    (
        2500,
        4,
        "E24",
        {
            "band1": "red",
            "band2": "yellow",
            "multiplier": "red",
            "preferred_value": 2400,
        },
    ),
    (
        5700000,
        4,
        "E24",
        {
            "band1": "green",
            "band2": "blue",
            "multiplier": "green",
            "preferred_value": 5600000,
        },
    ),
]


@pytest.mark.parametrize(
    "value, bands, series, expected", value_to_e12_four_band_test_data
)
def test_value_to_e12_4_band(value, bands, series, expected):
    result = resistor.value_to_bands(value, bands, series)
    assert result == expected


@pytest.mark.parametrize(
    "value, bands, series, expected", value_to_e24_four_band_test_data
)
def test_value_to_e24_4_band(value, bands, series, expected):
    result = resistor.value_to_bands(value, bands, series)
    assert result == expected


resistor_series_test_data = [
    (10000, 500, 10500),
    (2200, 2200, 4400),
    (580000, 10000, 590000),
]

resistor_parallel_test_data = [
    ((200, 470, 220), 85.67),
    ((330, 330), 165),
    ((1000, 1000, 1000, 1000), 250),
    ((100, 220, 330, 470), 50.75),
]


@pytest.mark.parametrize("r1, r2, expected", resistor_series_test_data)
def test_resistor_series(r1, r2, expected):
    result = resistor.calculate_series_resistance(r1, r2)
    assert result == expected


@pytest.mark.parametrize("values, expected", resistor_parallel_test_data)
def test_resistor_parallel(values, expected):
    result = resistor.calculate_parallel_resistance(*values)
    result = round(result, 2)
    assert result == expected


def test_resistor_series_exception():
    with pytest.raises(ValueError):
        resistor.calculate_series_resistance(1000)


def test_resistor_parallel_exception():
    with pytest.raises(ValueError):
        resistor.calculate_parallel_resistance(1000)


voltage_divider_test_data = [
    (1000, 1000, 12, 6),
    (2200, 1000, 5, 1.56),
    (470, 330, 25, 10.31),
    (100, 1000, 12, 10.91),
]

voltage_divider_resistor_test_data = [
    (12, 6, (10000, 10000, 0.5, 10000, 10000, 6)),
    (12, 3, (10000, 3333, 0.25, 10000, 3300, 2.98)),
    (10, 7.5, (3333, 10000, 0.75, 3300, 10000, 7.52)),
    (5, 1, (10000, 2500, 0.2, 10000, 2400, 0.97)),
]


@pytest.mark.parametrize("r1, r2, voltage, expected", voltage_divider_test_data)
def test_voltage_divider(r1, r2, voltage, expected):
    result = resistor.calculate_voltage_divider(r1, r2, voltage)
    result = round(result, 2)
    assert result == expected


@pytest.mark.parametrize("source, output, expected", voltage_divider_resistor_test_data)
def test_voltage_divider(source, output, expected):
    result = resistor.calculate_resistors_for_divider(source, output)
    r1 = round(result["r1"])
    r2 = round(result["r2"])
    ratio = round(result["voltage_ratio"], 2)

    preferred_r1 = round(result["preferred_values"]["r1"], 2)
    preferred_r2 = round(result["preferred_values"]["r2"], 2)
    preferred_v = round(result["preferred_values"]["output_voltage"], 2)

    assert r1 == expected[0]
    assert r2 == expected[1]
    assert ratio == expected[2]

    assert preferred_r1 == expected[3]
    assert preferred_r2 == expected[4]
    assert preferred_v == expected[5]
